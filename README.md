# Kubectl with envsubst docker image

Usage :

```yaml
job:
  image: gitlab-research.centralesupelec.fr:4567/docker-images/kubectl-envsubst/kubectl-envsubst
  script:
    - kubectl kustomize kustomize/overlays/${OVERLAY} | envsubst | kubectl apply -f -
  tags:
    - docker
```
