FROM alpine:3

LABEL org.opencontainers.image.authors="paul.bizouard@centralesupelec.fr"

RUN apk add --no-cache curl gettext \
    && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/

CMD ["/bin/sh"]
ENTRYPOINT [""]
